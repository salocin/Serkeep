from enum import Enum
import re


# TODO add multipart episode
class EpisodeFormat(Enum):
    SEASON_NUMBER_NUMBER_EPISODE_NUMBER_NUMBER = re.compile(r"(s\d\d)(e\d\d)", re.IGNORECASE)
    NUMBER_NUMBER_X_NUMBER_NUMBER = re.compile(r"(\d\d)x(\d\d)", re.IGNORECASE)
    NUMBER_X_NUMBER_NUMBER = re.compile(r"(\d)x(\d\d)", re.IGNORECASE)
