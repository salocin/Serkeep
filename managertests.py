import unittest
from manager import Manager


class ManagerTest(unittest.TestCase):
    manager = Manager("", "")

    def setUp(self):
        self.manager.series_names = ["NCIS",
                                     "NCIS Los Angeles",
                                     "NCIS New Orleans",
                                     "Blue Bloods",
                                     "Hawaii Five-0",
                                     "Bull",
                                     "The Mick"]

        self.manager.file_names = ["NCIS.S01E23.HDTV.LOL.mkv",
                                   "NCIS.New.Orleans.S01E23.HDTV.LOL.mkv"]

    def test_matches_series_correctly(self):
        # get matches
        matches1 = self.manager.find_possible_matches("NCIS.Los.Angeles.S08E09.HDTV.x264-LOL[ettv]")
        matches2 = self.manager.find_possible_matches("NCIS.S01E09.HDTV.x264-LOL.mkw")
        matches3 = self.manager.find_possible_matches(
            "The Mick - 01x13 - The Bully.AVS-SVA.English.C.updated.Addic7ed.com.srt")

        # get best match
        self.assertEqual("NCIS Los Angeles", self.manager.return_match(matches1))
        self.assertEqual("NCIS", self.manager.return_match(matches2))
        self.assertEqual("The Mick", self.manager.return_match(matches3))


    def test_case_insensitive(self):
        # get matches
        matches1 = self.manager.find_possible_matches("ncis.S08E09.HDTV.x264-LOL[ettv]")
        matches2 = self.manager.find_possible_matches("nCis.LoS.anGeleS.S08E09.HDTV.x264-LOL[ettv]")

        # get best match
        self.assertEqual("NCIS", self.manager.return_match(matches1))
        self.assertEqual("NCIS Los Angeles", self.manager.return_match(matches2))

    def test_extracts_season_correctly(self):
        # extract seasons from string
        season1 = self.manager.extract_season_from_file("ncis.S01E09.HDTV.x264-LOL[ettv]")
        season2 = self.manager.extract_season_from_file("NCIS.Los.Angeles.S02E09.HDTV.x264-LOL[ettv]")
        season3 = self.manager.extract_season_from_file("The Mick - 03x13 - The Bully.AVS-SVA.English.C.updated.Addic7ed.com.srt")
        # assert seasons strings to expected results
        self.assertEqual("Season 1", season1)
        self.assertEqual("Season 2", season2)
        self.assertEqual("Season 3", season3)

    def test_removes_leading_zeros(self):
        self.assertEqual("1", self.manager.remove_leading_zeros("01"))
        self.assertEqual("1", self.manager.remove_leading_zeros("001"))
        self.assertEqual("1", self.manager.remove_leading_zeros("0001"))
        self.assertEqual("S 1", self.manager.remove_leading_zeros("S 01"))
        self.assertEqual("Season 1", self.manager.remove_leading_zeros("Season 01"))

    def test_only_leading_zeros_are_removed(self):
        self.assertEqual("0", self.manager.remove_leading_zeros("0000"))
        self.assertEqual("0", self.manager.remove_leading_zeros("0"))

    def test_ignore_trailing_zeros(self):
        self.assertEqual("1000", self.manager.remove_leading_zeros("1000"))


if __name__ == '__main__':
    unittest.main()
