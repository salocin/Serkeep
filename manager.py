import os
import re
import shutil

from episodeformat import EpisodeFormat
from utility import RegexManager


class Manager(object):
    """
    This class has methods for handling moving episodes files to season and series folders
    and renaming file etc.
    """
    series_names = []
    file_names = []

    def __init__(self, working_directory, series_names_file):
        self.working_directory = working_directory
        self.series_names_file = series_names_file

    def read_series_names(self):
        """
        Reads series name from text file
        :return:
        """
        self.print_message("Reading series names from file")
        with open(self.series_names_file) as f:
            self.series_names = f.read().splitlines()

    def print_series_names(self):
        """
        Prints the series names to the standard output
        :return:
        """
        for name in self.series_names:
            print(name)

    def read_files_from_path(self):
        """
        Reads files and directories from the path and appends
        them to the file names list
        """
        print("\nreading files from path: ", self.working_directory, "\n")
        files = os.listdir(self.working_directory)
        [self.file_names.append(file) for file in files]

    def print_file_names(self):
        self.print_message("file names: ")
        for name in self.file_names:
            print(name)

    def create_series_folders(self):
        """
        Creates all series folders in the given path
        :return:
        """
        self.print_message("now creating series folders")
        for name in self.series_names:
            series_folder_path = os.path.join(self.working_directory, name)
            if not self.path_exists(series_folder_path):
                # create series folder
                os.mkdir(series_folder_path)
                print("created: ", series_folder_path)

    # TODO write test for this
    def move_episodes_to_series_folders(self):
        self.print_message("now moving episodes to series folders")
        for file in self.file_names:
            # ignore file which are series folders
            if not self.series_names.__contains__(file):
                # find matching series
                matches = self.find_possible_matches(file)
                if self.not_empty(matches):
                    matching_folder = self.return_match(matches)
                    self.move_episode(file, matching_folder)

    def move_episode(self, file, matching_folder):
        """
        Actually moves the episode from src to dest
        :param file: the file which should be moved
        :param matching_folder:  the series the file matched to
        :return:    src the source
                    dest the destination
        """
        src = os.path.join(self.working_directory, file)
        dest = os.path.join(self.working_directory, matching_folder, file)
        shutil.move(src, dest)

    def find_possible_matches(self, file):
        """
        Finds all possible series names matches for the given file
        and return them. Ignores case => converts everything to lowercase
        for comparision
        :param file: the file for which the matching should be done
        :return matches: the matches
        """
        matches = []
        for series_name in self.series_names:
            # split series name into tokens
            tokens = self.split_string_into_tokens(series_name)
            # if all tokens are in file then match is true
            match = all(token in file.lower() for token in tokens)
            if match:
                matches.append(series_name)
        return matches

    def return_match(self, matches):
        """
        Return the best matches from the matches list
        => sorts the matches and returns the element with first index of matches
        :param matches: the matches
        :return match: the best match
        """
        if self.not_empty(matches):
            self.sort_list_ascending(matches)
            return matches[0]
        else:
            return ""

    def create_season_folders(self):
        """
        Creates season folder when a season pattern can be extracted from the files in
        the working directory
        :return:
        """
        self.print_message("now creating season folders inside series folders")
        series_folders = [os.path.join(self.working_directory, name) for name in self.series_names]
        for folder in series_folders:
            series_folder_content = os.listdir(folder)
            for file in series_folder_content:
                season = self.extract_season_from_file(file)
                if season:
                    season_path = os.path.join(self.working_directory, folder, season)
                    # check if season path already exists
                    if not self.path_exists(season_path):
                        # create season folder
                        os.mkdir(season_path)
                    # move episode to season folder
                    self.move_episode_to_season_folder(file, folder, season_path)

    def move_episode_to_season_folder(self, file_name, folder, season_path):
        """
        Does move the episode to the corresponding season folder
        :param file_name: the name
        :param folder: the folder in which the file is
        :param season_path: the new path of the file
        """
        src = os.path.join(self.working_directory, folder, file_name)
        dest = os.path.join(season_path, file_name)
        shutil.move(src, dest)

    def extract_season_from_file(self, file_name: str):
        """
        Extracts the season from the epsiode file name
        :param file_name: the name
        :return match: the season it matched or else None
        """
        # try to match to first episode format
        search = None
        search = re.search(EpisodeFormat.SEASON_NUMBER_NUMBER_EPISODE_NUMBER_NUMBER.value, file_name)
        if not search:
            # try to match to second episode format
            search = re.search(EpisodeFormat.NUMBER_NUMBER_X_NUMBER_NUMBER.value, file_name)
        # if search found a match
        if search:
            # the first match group
            match = search.group(1)
            if "s" in match.lower():
                # replace s01 with Season 01
                match = self.replace_s_with_season(match)
            else:
                # replace 1 with Season 1
                match = self.suffix_season(match)
            # substitute Season 01 with Season 1 => remove leading zeros
            match = self.remove_leading_zeros(match)
            return match

    @staticmethod
    def print_message(text):
        print("\n\n%s\n\n" % text)

    @staticmethod
    def suffix_season(match):
        return match.replace(match, "Season " + match)

    @staticmethod
    def replace_s_with_season(match):
        return match.lower().replace("s", "Season ")

    @staticmethod
    def remove_leading_zeros(string):
        return re.sub(RegexManager.leading_zeros, r'\1', string)

    @staticmethod
    def path_exists(path):
        return os.path.exists(path)

    @staticmethod
    def split_string_into_tokens(series_name):
        return series_name.lower().split(" ")

    @staticmethod
    def sort_list_ascending(matches):
        matches.sort(key=lambda s: len(s), reverse=True)

    @staticmethod
    def not_empty(matches):
        return matches.__len__() > 0


if __name__ == '__main__':
    # prepare manager
    manager = Manager("Z:\BT-Downloads", "series_names.txt")
    manager.read_series_names()
    manager.read_files_from_path()
    # handling series folders
    manager.create_series_folders()
    manager.move_episodes_to_series_folders()
    # handling series season folders
    manager.create_season_folders()
